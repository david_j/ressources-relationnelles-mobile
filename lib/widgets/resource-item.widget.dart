import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ressources_relationnelles/config/GlobalConfig.dart';
import 'package:ressources_relationnelles/models/Resource.dart';

class ResourceItemWidget extends StatelessWidget {
  final Resource resource;

  ResourceItemWidget({this.resource});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              contentPadding: EdgeInsets.all(10),
              leading: Image(
                height: 35,
                image: NetworkImage(
                  (GlobalConfig().imageBaseUrl +
                      "/" +
                      resource.user.id +
                      '/' +
                      resource.user.profilePicture),
                ),
                errorBuilder: (BuildContext context, Object exception,
                    StackTrace stackTrace) {
                  return Text("");
                },
              ),
              title: Text(resource.title != null ? resource.title : ''),
              subtitle: Text(resource.message != null ? resource.message : ''),
            ),
            if (resource.image != null)
              Column(
                children: [
                  Image(
                      image: NetworkImage(
                        (GlobalConfig().imageBaseUrl +
                            "/" +
                            resource.id +
                            '/' +
                            resource.image),
                      ),
                      errorBuilder: (BuildContext context, Object exception,
                          StackTrace stackTrace) {
                        return Text("");
                      },
                      height: 75),
                ],
              ),
          ],
        ),
      ),
    );
  }
}
