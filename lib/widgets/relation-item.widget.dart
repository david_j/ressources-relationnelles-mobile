import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ressources_relationnelles/config/GlobalConfig.dart';
import 'package:ressources_relationnelles/models/Relation.dart';

class RelationItemWidget extends StatelessWidget {
  final Relation relation;

  RelationItemWidget({this.relation});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              leading: Image(
                height: 35,
                image: NetworkImage(
                  relation.relationUser.profilePicture != null
                      ? (GlobalConfig().imageBaseUrl +
                          "/" +
                          relation.relationUser.id +
                          '/' +
                          relation.relationUser.profilePicture)
                      : 'https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png',
                ),
                errorBuilder: (BuildContext context, Object exception,
                    StackTrace stackTrace) {
                  return Text("");
                },
              ),
              title: Text(relation.relationUser.firstName != null &&
                      relation.relationUser.lastName != null
                  ? relation.relationUser.firstName +
                      ' ' +
                      relation.relationUser.lastName
                  : ''),
            ),
            if (relation.image != null)
              Column(
                children: [
                  Image(
                      image: NetworkImage(
                        (GlobalConfig().imageBaseUrl +
                            "/relations/" +
                            relation.id +
                            '/' +
                            relation.image),
                      ),
                      errorBuilder: (BuildContext context, Object exception,
                          StackTrace stackTrace) {
                        return Text("");
                      },
                      height: 75),
                ],
              ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.end,
            //   children: <Widget>[
            //     TextButton(
            //       child: const Text('Voir'),
            //       onPressed: () {
            //         /* ... */
            //       },
            //     ),
            //     const SizedBox(width: 8),
            //     const SizedBox(width: 8),
            //   ],
            // ),
          ],
        ),
      ),
    );
  }
}
