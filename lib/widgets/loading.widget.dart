import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget {
  final String text;

  LoadingWidget({this.text});

  @override
  Widget build(BuildContext context) {
    return Expanded(
        flex: 10,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                  margin: new EdgeInsets.only(bottom: 20),
                  child: CircularProgressIndicator()),
              Text(text)
            ])
          ],
        ));
  }
}
