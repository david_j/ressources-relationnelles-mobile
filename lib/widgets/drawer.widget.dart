import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ressources_relationnelles/config/GlobalConfig.dart';
import 'package:ressources_relationnelles/models/User.dart';
import 'package:ressources_relationnelles/pages/contact.dart';
import 'package:ressources_relationnelles/pages/help.dart';
import 'package:ressources_relationnelles/pages/home.dart';
import 'package:ressources_relationnelles/pages/login.dart';
import 'package:ressources_relationnelles/pages/relations/relations.dart';
import 'package:ressources_relationnelles/pages/ressources/resources.dart';
import 'package:ressources_relationnelles/services/user.service.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../assets/styles/colors.dart';

class ResRelDrawer extends StatefulWidget {
  @override
  _ResRelDrawerState createState() => _ResRelDrawerState();
}

class _ResRelDrawerState extends State<ResRelDrawer> {
  void initState() {
    super.initState();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => {getCurrentUser(), getCurrentRoute()});
  }

  bool isLoading = false;
  User currentUser;

  bool isDrawerTabSelected(String tabName) {
    return ModalRoute.of(context).settings.name == tabName;
  }

  void getCurrentRoute() {
    print(ModalRoute.of(context).settings.name);
  }

  void getCurrentUser() async {
    setState(() {
      isLoading = true;
    });
    final response = await UserService().getCurrentUser();

    if (response.statusCode == 200) {
      User user = User.fromJson(jsonDecode(response.body));
      setState(() {
        currentUser = user;
        isLoading = false;
      });
    } else {
      print(response.body);
      setState(() {
        isLoading = false;
      });
    }
  }

  logout() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('token');
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => LoginPage()));
    // print('Déconnexion...');
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: ResRelColors.primary,
        child: ListView(
          padding: EdgeInsets.all(10),
          children: [
            Container(
              height: 125.0,
              child: DrawerHeader(
                child: Row(
                  children: [
                    Container(
                      margin: new EdgeInsets.only(right: 30),
                      // child: new Image(
                      //     image: new AssetImage(
                      //         'assets/images/icon_marianne.png'))),
                      child: currentUser != null
                          ? ClipRRect(
                              borderRadius: BorderRadius.circular(50.0),
                              child: Image(
                                  image: NetworkImage(
                                    (GlobalConfig().imageBaseUrl +
                                        "/" +
                                        currentUser.id +
                                        '/' +
                                        currentUser.profilePicture),
                                  ),
                                  errorBuilder: (BuildContext context,
                                      Object exception, StackTrace stackTrace) {
                                    return Text("");
                                  },
                                  height: 75),
                            )
                          : null,
                    ),
                    Text(
                      (!isLoading && currentUser != null)
                          ? currentUser.firstName + ' ' + currentUser.lastName
                          : '',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: new EdgeInsets.symmetric(vertical: 10),
              decoration: new BoxDecoration(
                color: isDrawerTabSelected('/home')
                    ? ResRelColors.primary
                    : ResRelColors.light,
                borderRadius: BorderRadius.circular(10),
                border: isDrawerTabSelected('/home')
                    ? Border.all(width: 2.0, color: ResRelColors.light)
                    : null,
              ),
              child: ListTile(
                onTap: () => {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => MyHomePage()))
                },
                leading: Icon(
                  Icons.home_rounded,
                  color: isDrawerTabSelected('/home')
                      ? ResRelColors.light
                      : ResRelColors.primary,
                  size: 30,
                ),
                title: Text(
                  'Accueil',
                  style: TextStyle(
                    color: isDrawerTabSelected('/home')
                        ? ResRelColors.light
                        : ResRelColors.primary,
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Container(
              margin: new EdgeInsets.symmetric(vertical: 10),
              decoration: new BoxDecoration(
                color: isDrawerTabSelected('/relations')
                    ? ResRelColors.primary
                    : ResRelColors.light,
                borderRadius: BorderRadius.circular(10),
                border: isDrawerTabSelected('/relations')
                    ? Border.all(width: 2.0, color: ResRelColors.light)
                    : null,
              ),
              child: ListTile(
                onTap: () => {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => RelationsPage()))
                },
                leading: Icon(
                  Icons.person,
                  color: isDrawerTabSelected('/relations')
                      ? ResRelColors.light
                      : ResRelColors.primary,
                  size: 30,
                ),
                title: Text(
                  'Relations',
                  style: TextStyle(
                    color: isDrawerTabSelected('/relations')
                        ? ResRelColors.light
                        : ResRelColors.primary,
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Container(
              margin: new EdgeInsets.symmetric(vertical: 10),
              decoration: new BoxDecoration(
                color: isDrawerTabSelected('/resources')
                    ? ResRelColors.primary
                    : ResRelColors.light,
                borderRadius: BorderRadius.circular(10),
                border: isDrawerTabSelected('/resources')
                    ? Border.all(width: 2.0, color: ResRelColors.light)
                    : null,
              ),
              child: ListTile(
                onTap: () => {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => ResourcePage()))
                },
                leading: Icon(
                  Icons.article,
                  color: isDrawerTabSelected('/resources')
                      ? ResRelColors.light
                      : ResRelColors.primary,
                  size: 30,
                ),
                title: Text(
                  'Ressources',
                  style: TextStyle(
                    color: isDrawerTabSelected('/resources')
                        ? ResRelColors.light
                        : ResRelColors.primary,
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Container(
              margin: new EdgeInsets.symmetric(vertical: 10),
              decoration: new BoxDecoration(
                color: isDrawerTabSelected('/help')
                    ? ResRelColors.primary
                    : ResRelColors.light,
                borderRadius: BorderRadius.circular(10),
                border: isDrawerTabSelected('/help')
                    ? Border.all(width: 2.0, color: ResRelColors.light)
                    : null,
              ),
              child: ListTile(
                onTap: () => {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => HelpPage()))
                },
                leading: Icon(
                  Icons.help,
                  color: isDrawerTabSelected('/help')
                      ? ResRelColors.light
                      : ResRelColors.primary,
                  size: 30,
                ),
                title: Text(
                  'Aide',
                  style: TextStyle(
                    color: isDrawerTabSelected('/help')
                        ? ResRelColors.light
                        : ResRelColors.primary,
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Container(
              margin: new EdgeInsets.symmetric(vertical: 10),
              decoration: new BoxDecoration(
                color: isDrawerTabSelected('/contact')
                    ? ResRelColors.primary
                    : ResRelColors.light,
                borderRadius: BorderRadius.circular(10),
                border: isDrawerTabSelected('/contact')
                    ? Border.all(width: 2.0, color: ResRelColors.light)
                    : null,
              ),
              child: ListTile(
                onTap: () => {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => ContactPage()))
                },
                leading: Icon(
                  Icons.phone,
                  color: isDrawerTabSelected('/contact')
                      ? ResRelColors.light
                      : ResRelColors.primary,
                  size: 30,
                ),
                title: Text(
                  'Contact',
                  style: TextStyle(
                    color: isDrawerTabSelected('/contact')
                        ? ResRelColors.light
                        : ResRelColors.primary,
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Container(
              margin: new EdgeInsets.symmetric(vertical: 10),
              decoration: new BoxDecoration(
                  color: Colors.red, borderRadius: BorderRadius.circular(10)),
              child: ListTile(
                onTap: logout,
                leading: Icon(
                  Icons.logout,
                  color: Colors.white,
                  size: 30,
                ),
                title: Text(
                  'Se déconnecter',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
