import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ressources_relationnelles/pages/profile.dart';
import 'package:ressources_relationnelles/pages/ressources/add-resource.dart';
import 'package:ressources_relationnelles/pages/settings.dart';

import '../assets/styles/colors.dart';

class ResRelNavigation extends StatefulWidget {
  @override
  _ResRelNavigationState createState() => _ResRelNavigationState();
}

class _ResRelNavigationState extends State<ResRelNavigation> {
  int currentIndex = 0;

  setBottomBarIndex(index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      height: 80,
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          CustomPaint(
            size: Size(size.width, 80),
            painter: BNBCustomPainter(),
          ),
          Center(
            heightFactor: 0.6,
            child: FloatingActionButton(
                backgroundColor: ResRelColors.primary,
                child: Icon(Icons.add_circle),
                elevation: 0.1,
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => AddResource()));
                }),
          ),
          Container(
            width: size.width,
            height: 80,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(
                  icon: Icon(
                    Icons.settings,
                    size: 35,
                    color: currentIndex == 1
                        ? ResRelColors.primary
                        : Colors.grey.shade400,
                  ),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SettingsPage()));
                    setBottomBarIndex(1);
                  },
                  splashColor: Colors.white,
                ),
                Container(
                  width: size.width * 0.10,
                ),
                IconButton(
                    icon: Icon(
                      Icons.person,
                      size: 35,
                      color: currentIndex == 3
                          ? ResRelColors.primary
                          : Colors.grey.shade400,
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProfilePage()));
                      setBottomBarIndex(3);
                    }),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class BNBCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = new Paint()
      ..color = Colors.white
      ..style = PaintingStyle.fill;

    Path path = Path();
    path.moveTo(0, 20); // Start
    path.quadraticBezierTo(size.width * 0.20, 0, size.width * 0.35, 0);
    path.quadraticBezierTo(size.width * 0.40, 0, size.width * 0.40, 20);
    path.arcToPoint(Offset(size.width * 0.60, 20),
        radius: Radius.circular(20.0), clockwise: false);
    path.quadraticBezierTo(size.width * 0.60, 0, size.width * 0.65, 0);
    path.quadraticBezierTo(size.width * 0.80, 0, size.width, 20);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.lineTo(0, 20);
    canvas.drawShadow(path, Colors.black, 5, true);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
