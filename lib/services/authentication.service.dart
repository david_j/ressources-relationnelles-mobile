import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http_interceptor/http_interceptor.dart';
import 'package:ressources_relationnelles/config/GlobalConfig.dart';
import 'package:ressources_relationnelles/xhr/xhr-interceptor.dart';

String baseUrl = GlobalConfig().apiBaseUrl;

class AuthenticationService {
  Future<http.Response> register(Object registerUser) async {
    print(jsonEncode(registerUser));
    final httpClient = InterceptedHttp.build(interceptors: [XhrInterceptor()]);
    return httpClient.post(
      Uri.parse(baseUrl + '/api/register'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(registerUser),
    );
  }

  Future<http.Response> login(Object credentials) async {
    print(baseUrl);
    final httpClient = InterceptedHttp.build(interceptors: [XhrInterceptor()]);
    return httpClient.post(
      Uri.parse(baseUrl + '/api/login'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(credentials),
    );
  }

  Future<http.Response> isAllowed(String role) {
    final httpClient = InterceptedHttp.build(interceptors: [XhrInterceptor()]);
    return httpClient.get(Uri.parse(baseUrl + '/api/authentication/is-allowed' + role),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        });
  }
}
