import 'package:flutter/material.dart';

class UtilityService {

showAlert(context, content) async {
  await Future.delayed(Duration(milliseconds: 50));
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return new AlertDialog(content: content);
      });
}

}