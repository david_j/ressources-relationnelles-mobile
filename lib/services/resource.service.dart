import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http_interceptor/http/intercepted_http.dart';
import 'package:http_interceptor/http_interceptor.dart';
import 'package:ressources_relationnelles/config/GlobalConfig.dart';
import 'package:ressources_relationnelles/xhr/xhr-interceptor.dart';

String baseUrl = GlobalConfig().apiBaseUrl;

class ResourceService {
  Future<http.Response> findAllByCurrentUserRelations() async {
    final httpClient = InterceptedHttp.build(interceptors: [XhrInterceptor()]);
    return httpClient.get(
        Uri.parse(baseUrl + '/api/resources/by-current-user-relations'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        });
  }

  Future<http.Response> findOneById(String resourceId) async {
    final httpClient = InterceptedHttp.build(interceptors: [XhrInterceptor()]);
    return httpClient.get(Uri.parse(baseUrl + '/api/resources/' + resourceId),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        });
  }

  Future<http.Response> create(resource) async {
    print(jsonEncode(resource));
    final httpClient = InterceptedHttp.build(interceptors: [XhrInterceptor()]);
    return httpClient.post(Uri.parse(baseUrl + '/api/resources'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8'
        },
        body: jsonEncode(resource));
  }
}
