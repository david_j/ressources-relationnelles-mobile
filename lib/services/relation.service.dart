
import 'package:http/http.dart' as http;
import 'package:http_interceptor/http/intercepted_http.dart';
import 'package:http_interceptor/http_interceptor.dart';
import 'package:ressources_relationnelles/config/GlobalConfig.dart';
import 'package:ressources_relationnelles/xhr/xhr-interceptor.dart';

String baseUrl = GlobalConfig().apiBaseUrl;

class RelationService {
  Future<http.Response> findAllByCurrentUser() async {
    final httpClient = InterceptedHttp.build(interceptors: [XhrInterceptor()]);
    return httpClient.get(Uri.parse(baseUrl + '/api/relations/by-current-user'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        });
  }

  Future<http.Response> findOneById(String resourceId) async {
    final httpClient = InterceptedHttp.build(interceptors: [XhrInterceptor()]);
    return httpClient.get(Uri.parse(baseUrl + '/api/relations/' + resourceId),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        });
  }

  Future<http.Response> sendRelationRequest() async {
    final httpClient = InterceptedHttp.build(interceptors: [XhrInterceptor()]);
    return httpClient.get(Uri.parse(baseUrl + '/api/relation-requests'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8'
        });
  }
}
