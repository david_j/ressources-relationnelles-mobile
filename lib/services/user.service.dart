import 'package:http/http.dart' as http;
import 'package:http_interceptor/http/intercepted_http.dart';
import 'package:http_interceptor/http_interceptor.dart';
import 'package:ressources_relationnelles/config/GlobalConfig.dart';
import 'package:ressources_relationnelles/xhr/xhr-interceptor.dart';

String baseUrl = GlobalConfig().apiBaseUrl;

class UserService {

  Future<http.Response> getCurrentUser() async {
    final httpClient = InterceptedHttp.build(interceptors: [XhrInterceptor()]);
    return httpClient.get(Uri.parse(baseUrl + '/api/users/current'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        });
  }

}
