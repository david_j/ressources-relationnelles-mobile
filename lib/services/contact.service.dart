import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http_interceptor/http/intercepted_http.dart';
import 'package:http_interceptor/http_interceptor.dart';
import 'package:ressources_relationnelles/config/GlobalConfig.dart';
import 'package:ressources_relationnelles/xhr/xhr-interceptor.dart';

String baseUrl = GlobalConfig().apiBaseUrl;

class ContactService {
  Future<http.Response> sendHelpContactMessage(message) async {
    final httpClient = InterceptedHttp.build(interceptors: [XhrInterceptor()]);
    return httpClient.post(Uri.parse(baseUrl + '/api/chat/help'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8'
        },
        body: jsonEncode(message));
  }
}
