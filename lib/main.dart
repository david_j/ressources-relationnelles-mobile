import 'package:flutter/material.dart';
import 'package:ressources_relationnelles/pages/contact.dart';
import 'package:ressources_relationnelles/pages/help.dart';
import 'package:ressources_relationnelles/pages/login.dart';
import 'package:ressources_relationnelles/pages/profile.dart';
import 'package:ressources_relationnelles/pages/register.dart';
import 'package:ressources_relationnelles/pages/relations/relations.dart';
import 'package:ressources_relationnelles/pages/ressources/add-resource.dart';
import 'package:ressources_relationnelles/pages/ressources/resources.dart';
import 'package:ressources_relationnelles/pages/settings.dart';

import 'pages/home.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ressources Relationnelles',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.indigo,
      ),
      home: MyHomePage(title: 'Ressources Relationnelles'),
      initialRoute: '/',
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.
        '/home': (context) => MyHomePage(),
        '/resources': (context) => ResourcePage(),
        '/add-resource': (context) => AddResource(),
        '/relations': (context) => RelationsPage(),
        '/contact': (context) => ContactPage(),
        '/help': (context) => HelpPage(),
        '/login': (context) => LoginPage(),
        '/register': (context) => RegisterPage(),
        '/profile': (context) => ProfilePage(),
        '/settings': (context) => SettingsPage(),
      },
    );
  }
}
