import 'package:flutter/material.dart';

class ResRelColors {
  static Color primary = Color.fromARGB(255, 80, 111, 196);
  static Color secondary = Color.fromARGB(255, 171, 184, 222);
  static Color tertiary = Color.fromARGB(255, 34, 150, 126);
  static Color success = Color.fromARGB(255, 70, 198, 98);
  static Color warning = Color.fromARGB(255, 255, 172, 39);
  static Color danger = Color.fromARGB(255, 252, 112, 106);
  static Color info = Color.fromARGB(255, 23, 162, 184);
  static Color light = Color.fromARGB(255, 255, 255, 255);
  static Color dark = Color.fromARGB(255, 0, 0, 0);
}
