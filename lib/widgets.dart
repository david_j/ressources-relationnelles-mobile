import 'package:flutter/material.dart';

class MarianneIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    var assetsImage = new AssetImage('assets/images/icon_marianne.png');
    var image = new Image(image: assetsImage, width: 256.0, height: 256.0);
    return Container(child: image);
  }
}
