import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:ressources_relationnelles/assets/styles/colors.dart';
import 'package:ressources_relationnelles/models/User.dart';
import 'package:ressources_relationnelles/services/user.service.dart';
import 'package:ressources_relationnelles/widgets/bottom-navigation-bar.widget.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => getCurrentUser());
  }

  final usernameController = TextEditingController();
  final emailController = TextEditingController();
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final birthDateController = TextEditingController();
  final phoneController = TextEditingController();
  final genreController = TextEditingController();
  final addressController = TextEditingController();
  final cityController = TextEditingController();
  final postalCodeController = TextEditingController();

  bool checkboxValue = false;

  bool isLoading = false;
  User currentUser;
  bool isEditing = false;

  static const dateFormat = 'dd/MM/yyyy';

  String formattedDate(DateTime dateTime) {
    return DateFormat(dateFormat).format(dateTime);
  }

  void getCurrentUser() async {
    setState(() {
      isLoading = true;
    });
    final response = await UserService().getCurrentUser();
    print(response);

    if (response.statusCode == 200) {
      User user = User.fromJson(jsonDecode(response.body));
      print(user);
      setState(() {
        currentUser = user;
        usernameController.text = user.username;
        firstNameController.text = user.firstName;
        lastNameController.text = user.lastName;
        emailController.text = user.email;
        birthDateController.text = user.birthDate;
        cityController.text = user.city;
        postalCodeController.text = user.postalCode;
        addressController.text = user.address;
        isLoading = false;
      });
    } else {
      print(response.body);
      setState(() {
        currentUser = null;
        isLoading = false;
      });
    }
  }

  void toggleEditProfile() {
    setState(() {
      isEditing = !isEditing;
    });
  }

  void updateProfile() {
    setState(() {
      isLoading = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ResRelColors.primary,
        title: Text("Mon Profil"),
      ),
      bottomNavigationBar: ResRelNavigation(),
      body: Container(
        child: ListView(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 350,
                  height: 455,
                  child: Card(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Icon(Icons.person, size: 96),
                            Padding(
                              padding: EdgeInsets.only(right: 5),
                            ),
                            Flexible(
                              child: TextField(
                                controller: firstNameController,
                                enabled: isEditing,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: currentUser.firstName,
                                  hintText: "Prénom",
                                  focusColor: ResRelColors.primary,
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 10),
                            ),
                            Flexible(
                              child: TextField(
                                controller: lastNameController,
                                enabled: isEditing,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: currentUser.lastName,
                                  hintText: "Nom",
                                  focusColor: ResRelColors.primary,
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 10),
                            ),
                          ],
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(15, 0, 10, 0),
                          child: TextField(
                            controller: emailController,
                            enabled: isEditing,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: currentUser.email,
                              hintText: "Adresse email",
                              focusColor: ResRelColors.primary,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(15, 10, 10, 0),
                          child: TextField(
                            controller: usernameController,
                            enabled: isEditing,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: currentUser.username,
                              hintText: "Nom d'utilisateur",
                              focusColor: ResRelColors.primary,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(15, 10, 10, 0),
                          child: TextField(
                            controller: birthDateController,
                            enabled: isEditing,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: currentUser.birthDate != null
                                    ? formattedDate(
                                        DateTime.parse(currentUser.birthDate))
                                    : null,
                                focusColor: ResRelColors.primary,
                                hintText: "Date de naissance (DD-MM-YYYY)"),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(15, 10, 10, 0),
                          child: TextField(
                            controller: addressController,
                            enabled: isEditing,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: currentUser.address,
                              hintText: "Adresse",
                              focusColor: ResRelColors.primary,
                            ),
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(bottom: 10)),
                        Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.fromLTRB(5, 0, 10, 0),
                            ),
                            Flexible(
                              child: TextField(
                                controller: postalCodeController,
                                enabled: isEditing,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: currentUser.postalCode,
                                  hintText: "Code postal",
                                  focusColor: ResRelColors.primary,
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 10),
                            ),
                            Flexible(
                              child: TextField(
                                controller: cityController,
                                enabled: isEditing,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: currentUser.city,
                                  hintText: "Ville",
                                  focusColor: ResRelColors.primary,
                                ),
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(right: 10)),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: toggleEditProfile,
        label: Text(isEditing == true ? 'Annuler' : 'Éditer'),
        icon: Icon(isEditing == true ? Icons.cancel : Icons.edit),
        backgroundColor: ResRelColors.primary,
      ),
    );
  }
}
