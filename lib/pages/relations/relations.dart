import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ressources_relationnelles/assets/styles/colors.dart';
import 'package:ressources_relationnelles/models/Relation.dart';
import 'package:ressources_relationnelles/services/relation.service.dart';
import 'package:ressources_relationnelles/services/utility.service.dart';
import 'package:ressources_relationnelles/widgets/drawer.widget.dart';
import 'package:ressources_relationnelles/widgets/loading.widget.dart';
import 'package:ressources_relationnelles/widgets/relation-item.widget.dart';

class RelationsPage extends StatefulWidget {
  RelationsPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _RelationsPageState createState() => _RelationsPageState();
}

class _RelationsPageState extends State<RelationsPage> {
  void initState() {
    super.initState();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => findAllRelationsByCurrentUser());
  }

  List<dynamic> relationList = [];
  bool isLoading = false;

  void findAllRelationsByCurrentUser() async {
    setState(() {
      isLoading = true;
    });
    final response = await RelationService().findAllByCurrentUser();

    if (response.statusCode == 200) {
      print(response.body);
      List<Relation> relationJsonList = parseRelations(response.body);
      setState(() {
        relationList = relationJsonList;
      });
      setState(() {
        isLoading = false;
      });
    } else {
      UtilityService().showAlert(
          context,
          new Text(
              "Une erreur s'est produite durant la récupération des ressources"));
    }
    setState(() {
      isLoading = false;
    });
  }

  List<Relation> parseRelations(String responseBody) {
    final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

    return parsed.map<Relation>((json) => Relation.fromJson(json)).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: ResRelColors.primary, title: Text("Relations")),
      drawer: ResRelDrawer(),
      body: Container(
          child: Column(children: [
        isLoading
            ? LoadingWidget(
                text: "Chargement des relations...",
              )
            : Expanded(
                child: GridView.count(
                // Create a grid with 2 columns. If you change the scrollDirection to
                // horizontal, this produces 2 rows.
                crossAxisCount: 2,
                // Generate 100 widgets that display their index in the List.
                children: List.generate(relationList.length, (index) {
                  return Center(
                    child: RelationItemWidget(
                      relation: relationList[index],
                    ),
                    // ),
                  );
                }),
              )
                // child: ListView(
                //     children: List.generate(relationList.length, (index) {
                //   return Container(
                //     padding: EdgeInsets.all(10),
                //     height: 220,
                //     width: double.maxFinite,
                //     child: RelationItemWidget(
                //       relation: relationList[index],
                //     ),
                //   );
                // })),
                )
      ])),
    );
  }
}
