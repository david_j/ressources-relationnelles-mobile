import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ressources_relationnelles/assets/styles/colors.dart';
import 'package:ressources_relationnelles/pages/ressources/resources.dart';
import 'package:ressources_relationnelles/services/resource.service.dart';
import 'package:ressources_relationnelles/services/utility.service.dart';
import 'package:ressources_relationnelles/widgets/drawer.widget.dart';

class AddResource extends StatefulWidget {
  AddResource({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _AddResourceState createState() => _AddResourceState();
}

class _AddResourceState extends State<AddResource> {
  final titleController = TextEditingController();
  final messageController = TextEditingController();
  final linkController = TextEditingController();
  final imageController = TextEditingController();

  uploadImage() async {
    //
  }

  createResource() async {
    Map<String, dynamic> resourceToAdd = {
      'resource': {
        'title': titleController.text,
        'message': messageController.text,
        'link': linkController.text,
      }
    };
    print(resourceToAdd);
    final response = await ResourceService().create(resourceToAdd);

    if (response.statusCode == 200) {
      Map<String, dynamic> jsonResponse = json.decode(response.body);
      // print(jsonResponse['token']);
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => ResourcePage()));

      Widget content = new Text("Ressource créée avec succès !");
      UtilityService().showAlert(context, content);
    } else {
      print(response.body);
      Widget content = new Text(
          "Une erreur s'est produite durant la création de la ressource (Erreur ${response.statusCode})");
      UtilityService().showAlert(context, content);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: ResRelColors.primary,
          title: Text("Ajouter une ressource")),
      drawer: ResRelDrawer(),
      body: Container(
        child: ListView(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 350,
                  height: 630,
                  child: Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.fromLTRB(15, 15, 15, 1),
                          child: TextField(
                            controller: titleController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Titre',
                              focusColor: ResRelColors.primary,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(15, 15, 15, 1),
                          child: TextField(
                            controller: messageController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Message',
                              focusColor: ResRelColors.primary,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(15, 15, 15, 1),
                          child: TextField(
                            controller: linkController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Lien',
                              focusColor: ResRelColors.primary,
                            ),
                          ),
                        ),
                        Container(
                            padding: EdgeInsets.fromLTRB(15, 15, 15, 1),
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                    ResRelColors.light,
                                  ),
                                  foregroundColor: MaterialStateProperty.all(
                                      ResRelColors.primary)),
                              child: Text("Ajouter une image"),
                              onPressed: uploadImage,
                            )),
                        Container(
                            padding: EdgeInsets.fromLTRB(15, 15, 15, 1),
                            child: ElevatedButton(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(Icons.add),
                                  Text("Ajouter la resource")
                                ],
                              ),
                              style: ButtonStyle(
                                  padding: MaterialStateProperty.all(
                                      EdgeInsets.fromLTRB(60, 20, 60, 20)),
                                  backgroundColor: MaterialStateProperty.all(
                                      ResRelColors.primary)),
                              onPressed: createResource,
                            )),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
