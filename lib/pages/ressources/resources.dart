import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ressources_relationnelles/assets/styles/colors.dart';
import 'package:ressources_relationnelles/models/Resource.dart';
import 'package:ressources_relationnelles/services/resource.service.dart';
import 'package:ressources_relationnelles/services/utility.service.dart';
import 'package:ressources_relationnelles/widgets/bottom-navigation-bar.widget.dart';
import 'package:ressources_relationnelles/widgets/drawer.widget.dart';
import 'package:ressources_relationnelles/widgets/loading.widget.dart';
import 'package:ressources_relationnelles/widgets/resource-item.widget.dart';

import '../../services/resource.service.dart';

class ResourcePage extends StatefulWidget {
  ResourcePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ResourcePageState createState() => _ResourcePageState();
}

class _ResourcePageState extends State<ResourcePage> {
  void initState() {
    super.initState();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => findAllResourcesByCurrentUserRelations());
  }

  bool checkboxValue = false;
  List<dynamic> resourceList = [];
  bool isLoading = false;

  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  showAlert(content) async {
    await Future.delayed(Duration(milliseconds: 50));
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return new AlertDialog(content: content);
        });
  }

  List<Resource> parseResources(String responseBody) {
    final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

    return parsed.map<Resource>((json) => Resource.fromJson(json)).toList();
  }

  void findAllResourcesByCurrentUserRelations() async {
    setState(() {
      isLoading = true;
    });
    final response = await ResourceService().findAllByCurrentUserRelations();

    if (response.statusCode == 200) {
      List<Resource> resourceJsonList = parseResources(response.body);
      setState(() {
        resourceList = resourceJsonList;
      });
      setState(() {
        isLoading = false;
      });
    } else {
      UtilityService().showAlert(
          context,
          new Text(
              "Une erreur s'est produite durant la récupération des ressources"));
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ResRelColors.primary,
        title: Text("Ressources"),
      ),
      drawer: ResRelDrawer(),
      bottomNavigationBar: ResRelNavigation(),
      body: Container(
          child: Column(children: [
        isLoading
            ? LoadingWidget(
                text: "Chargement des ressoures...",
              )
            : Expanded(
                child: ListView(
                    children: List.generate(resourceList.length, (index) {
                  return Container(
                    padding: EdgeInsets.all(10),
                    height: 220,
                    width: double.maxFinite,
                    child: ResourceItemWidget(
                      resource: resourceList[index],
                    ),
                  );
                })),
              )
      ])),
      floatingActionButton: FloatingActionButton(
          onPressed: findAllResourcesByCurrentUserRelations,
          child: Icon(Icons.refresh)),
    );
  }
}
