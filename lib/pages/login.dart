import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ressources_relationnelles/assets/styles/colors.dart';
import 'package:ressources_relationnelles/pages/home.dart';
import 'package:ressources_relationnelles/pages/register.dart';
import 'package:ressources_relationnelles/services/authentication.service.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../services/authentication.service.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool checkboxValue = false;

  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  showAlert(content) async {
    await Future.delayed(Duration(milliseconds: 50));
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return new AlertDialog(content: content);
        });
  }

  void login() async {
    String email = emailController.text;
    String password = passwordController.text;
    Map<String, dynamic> credentials = {'email': email, 'password': password};

    final response = await AuthenticationService().login(credentials);

    if (response.statusCode == 200) {
      Map<String, dynamic> jsonResponse = json.decode(response.body);
      // print(jsonResponse['token']);
      final prefs = await SharedPreferences.getInstance();
      prefs.setString('token', jsonResponse['token']);
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => MyHomePage()));

      // Widget content = new Text("Connexion réussie !");
      // showAlert(content);

    } else {
      Widget content =
          new Text("Une erreur s'est produite durant la connexion");
      showAlert(content);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ResRelColors.primary,
        title: Text("Connexion"),
      ),
      body: Container(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.all(20),
                    child: Text('Se connecter',
                        style: TextStyle(
                            fontSize: 24, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 350,
                  height: 300,
                  child: Card(
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.all(15),
                          child: TextField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Adresse email',
                              focusColor: ResRelColors.primary,
                            ),
                            controller: emailController,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(15),
                          child: TextField(
                            obscureText: true,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Mot de Passe',
                              focusColor: ResRelColors.primary,
                            ),
                            controller: passwordController,
                          ),
                        ),
                        Row(
                          children: [
                            Checkbox(
                              value: this.checkboxValue,
                              activeColor: ResRelColors.primary,
                              onChanged: (bool value) {
                                setState(() {
                                  this.checkboxValue = value;
                                });
                              },
                            ),
                            Text(
                              'Se souvenir de moi',
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ElevatedButton(
                              onPressed: login,
                              style: ElevatedButton.styleFrom(
                                onPrimary: Colors.white,
                                primary: ResRelColors.primary,
                              ),
                              child: Text("Se connecter"),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  'Pas encore inscrit ?',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RegisterPage()));
                  },
                  child: Text(
                    "S'inscire",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: ResRelColors.primary,
                    ),
                  ),
                ),
                Padding(padding: EdgeInsets.only(right: 20)),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
