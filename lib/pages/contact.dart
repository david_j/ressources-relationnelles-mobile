import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ressources_relationnelles/assets/styles/colors.dart';
import 'package:ressources_relationnelles/services/contact.service.dart';
import 'package:ressources_relationnelles/services/utility.service.dart';
import 'package:ressources_relationnelles/widgets/drawer.widget.dart';

class ContactPage extends StatefulWidget {
  ContactPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ContactPageState createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  final titleController = TextEditingController();
  final messageController = TextEditingController();

  sendMessage() async {
    Map<String, dynamic> contactMessage = {
      'title': titleController.text,
      'reason': messageController.text,
    };
    print(contactMessage);
    final response =
        await ContactService().sendHelpContactMessage(contactMessage);
    print(response.statusCode);
    if (response.statusCode == 201) {
      UtilityService()
          .showAlert(context, new Text("Message envoyé avec succès !"));
      Navigator.pushReplacementNamed(context, '/home');
      print(response.body);
    } else {
      UtilityService().showAlert(context,
          new Text("Une erreur s'est produite lors de l'envoi du message"));
      print(response.body);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: ResRelColors.primary,
          title: Text("Contact"),
        ),
        drawer: ResRelDrawer(),
        body: Container(
          child: ListView(children: [
            Padding(
              padding: new EdgeInsets.all(30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                    Column(
                      children: [
                        Icon(
                          Icons.message_outlined,
                          color: ResRelColors.primary,
                          size: 42,
                        ),
                        Text("Nous contacter", style: TextStyle(fontSize: 30)),
                      ],
                    ),
                  ]),
                  Divider(
                    height: 20,
                    thickness: 1,
                    indent: 20,
                    endIndent: 20,
                    color: ResRelColors.primary,
                  )
                ],
              ),
            ),
            Padding(
                padding: new EdgeInsets.all(30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(15, 15, 15, 1),
                      child: TextField(
                        controller: titleController,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Raison',
                          focusColor: ResRelColors.primary,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(15, 15, 15, 1),
                      child: TextField(
                        minLines: 4,
                        maxLines: 10,
                        controller: messageController,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Message',
                          focusColor: ResRelColors.primary,
                        ),
                      ),
                    ),
                    Container(
                        padding: EdgeInsets.fromLTRB(15, 15, 15, 1),
                        child: ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                  ResRelColors.primary)),
                          onPressed: sendMessage,
                          child: Text('Envoyer',
                              style: TextStyle(
                                  fontSize: 20, color: ResRelColors.light)),
                        )),
                  ],
                ))
          ]),
        ),
      ),
    );
  }
}
