import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ressources_relationnelles/assets/styles/colors.dart';
import 'package:ressources_relationnelles/pages/contact.dart';
import 'package:ressources_relationnelles/widgets/drawer.widget.dart';

class HelpPage extends StatefulWidget {
  HelpPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HelpPageState createState() => _HelpPageState();
}

class _HelpPageState extends State<HelpPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
          AppBar(backgroundColor: ResRelColors.primary, title: Text("Aide")),
      drawer: ResRelDrawer(),
      body: Container(
        child: Padding(
          padding: new EdgeInsets.all(30),
          child: (Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Column(
                  children: [
                    Icon(
                      Icons.help_outlined,
                      color: ResRelColors.primary,
                      size: 42,
                    ),
                    Text("Besoin d'aide ?", style: TextStyle(fontSize: 30)),
                  ],
                ),
              ]),
            ],
          )),
        ),
      ),
      floatingActionButton: Container(
        child: FittedBox(
          child: ElevatedButton(
            onPressed: () => Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => ContactPage())),
            child: Row(children: [
              Padding(
                padding: new EdgeInsets.only(right: 10),
                child: Icon(
                  Icons.message_outlined,
                  size: 22,
                ),
              ),
              Text('Nous contacter'),
            ]),
            style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(ResRelColors.primary)),
          ),
        ),
      ),
    );
  }
}
