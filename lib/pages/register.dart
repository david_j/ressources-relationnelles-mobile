import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ressources_relationnelles/assets/styles/colors.dart';
import 'package:ressources_relationnelles/pages/login.dart';
import 'package:ressources_relationnelles/services/authentication.service.dart';
import 'package:ressources_relationnelles/services/utility.service.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final usernameController = TextEditingController();
  final emailController = TextEditingController();
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final birthDateController = TextEditingController();
  final phoneController = TextEditingController();
  final genreController = TextEditingController();
  final addressController = TextEditingController();
  final cityController = TextEditingController();
  final postalCodeController = TextEditingController();

  final passwordController = TextEditingController();
  final passwordConfirmController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    usernameController.dispose();
    emailController.dispose();
    firstNameController.dispose();
    lastNameController.dispose();
    birthDateController.dispose();
    phoneController.dispose();
    genreController.dispose();
    addressController.dispose();
    cityController.dispose();
    postalCodeController.dispose();
    super.dispose();
  }

  void register() async {
    Map<String, dynamic> registerUser = {
      'username': usernameController.text,
      'email': emailController.text,
      'firstName': firstNameController.text,
      'lastName': lastNameController.text,
      'birthDate': birthDateController.text,
      'phone': phoneController.text,
      'genre': genreController.text,
      'address': addressController.text,
      'city': cityController.text,
      'postalCode': postalCodeController.text,
      'password': passwordController.text,
    };
    print(registerUser);
    if (passwordController.text != passwordConfirmController.text) {
      UtilityService().showAlert(
          context, new Text('Les mots de passe ne correspondent pas'));
    }
    final response = await AuthenticationService().register(registerUser);
    print(response.statusCode);
    if (response.statusCode == 201) {
      UtilityService()
          .showAlert(context, new Text("Inscription effectuée avec succès !"));
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => LoginPage()));
      print(response.body);
    } else {
      UtilityService().showAlert(
          context, new Text("Une erreur s'est produite lors de l'inscription"));
      print(response.body);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ResRelColors.primary,
        title: Text("Inscription"),
      ),
      body: Container(
        child: ListView(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 350,
                  height: 630,
                  child: Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.fromLTRB(15, 15, 15, 1),
                          child: TextField(
                            controller: emailController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Adresse email',
                              focusColor: ResRelColors.primary,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                          child: TextField(
                            controller: usernameController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Nom d'utilisateur",
                              focusColor: ResRelColors.primary,
                            ),
                          ),
                        ),
                        Row(
                          children: [
                            Padding(padding: EdgeInsets.only(left: 15)),
                            Flexible(
                              child: TextField(
                                controller: firstNameController,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: "Prénom",
                                  focusColor: ResRelColors.primary,
                                ),
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(right: 5)),
                            Flexible(
                              child: TextField(
                                controller: lastNameController,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: "Nom",
                                  focusColor: ResRelColors.primary,
                                ),
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(right: 15)),
                          ],
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(15, 15, 15, 1),
                          child: TextField(
                            controller: birthDateController,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: "Date de naissance",
                                focusColor: ResRelColors.primary,
                                hintText: "JJ-MM-AAAA"),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                          child: TextField(
                            controller: addressController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Adresse",
                              focusColor: ResRelColors.primary,
                            ),
                          ),
                        ),
                        Row(
                          children: [
                            Padding(padding: EdgeInsets.only(left: 15)),
                            Flexible(
                              child: TextField(
                                controller: cityController,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: "Ville",
                                  focusColor: ResRelColors.primary,
                                ),
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(right: 5)),
                            Flexible(
                              child: TextField(
                                controller: postalCodeController,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: "Code postal",
                                  focusColor: ResRelColors.primary,
                                ),
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(right: 15)),
                          ],
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(15, 15, 15, 1),
                          child: TextField(
                            controller: passwordController,
                            obscureText: true,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Mot de passe",
                              focusColor: ResRelColors.primary,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                          child: TextField(
                            controller: passwordConfirmController,
                            obscureText: true,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "Confirmation du mot de passe",
                              focusColor: ResRelColors.primary,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          // Add your onPressed code here!
        },
        label: TextButton(
          onPressed: register,
          child: Text(
            "S'inscrire",
            style: TextStyle(color: ResRelColors.light, fontSize: 18),
          ),
        ),
        icon: Icon(Icons.check_rounded),
        foregroundColor: ResRelColors.light,
        backgroundColor: ResRelColors.primary,
      ),
    );
  }
}
