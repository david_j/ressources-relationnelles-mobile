import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ressources_relationnelles/assets/styles/colors.dart';
import 'package:ressources_relationnelles/models/User.dart';
import 'package:ressources_relationnelles/pages/login.dart';
import 'package:ressources_relationnelles/pages/register.dart';
import 'package:ressources_relationnelles/services/user.service.dart';
import 'package:ressources_relationnelles/widgets.dart';
import 'package:ressources_relationnelles/widgets/bottom-navigation-bar.widget.dart';
import 'package:ressources_relationnelles/widgets/drawer.widget.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => getCurrentUser());
  }

  bool isLoading = false;
  User currentUser;

  void getCurrentUser() async {
    setState(() {
      isLoading = true;
    });
    final response = await UserService().getCurrentUser();
    print(response);

    if (response.statusCode == 200) {
      User user = User.fromJson(jsonDecode(response.body));
      print(user);
      setState(() {
        currentUser = user;
        isLoading = false;
      });
    } else {
      print(response.body);
      setState(() {
        currentUser = null;
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ResRelColors.primary,
        title: Text("Accueil"),
      ),
      drawer: (!isLoading && currentUser != null) ? ResRelDrawer() : null,
      bottomNavigationBar:
          (!isLoading && currentUser != null) ? ResRelNavigation() : null,
      body: Container(
        color: Color.fromRGBO(245, 245, 245, 1),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.all(20),
                    child: Text('Ressources Relationnelles',
                        style: TextStyle(
                            fontSize: 24, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center),
                  ),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                new MarianneIcon(),
              ],
            ),
            if (!isLoading && currentUser == null)
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(padding: EdgeInsets.only(top: 70)),
                  Spacer(),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RegisterPage()));
                    },
                    style: ElevatedButton.styleFrom(
                      onPrimary: Colors.white,
                      primary: ResRelColors.primary,
                    ),
                    child: Text("S'inscrire"),
                  ),
                  Spacer(),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => LoginPage()));
                    },
                    style: ElevatedButton.styleFrom(
                      onPrimary: ResRelColors.primary,
                      primary: Colors.white,
                    ),
                    child: Text("Se connecter"),
                  ),
                  Spacer(),
                ],
              ),
            if (!isLoading && currentUser != null)
              Row(
                children: [
                  Expanded(
                      child: Padding(
                          padding: EdgeInsets.all(20),
                          child: Card(
                            child: Padding(
                              padding: new EdgeInsets.all(20),
                              child: Text(
                                  'Bienvenue, ' +
                                      currentUser.firstName +
                                      ' ' +
                                      currentUser.lastName,
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center),
                            ),
                          ))),
                ],
              ),
          ],
        ),
      ),
    );
  }
}
