import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ressources_relationnelles/assets/styles/colors.dart';
import 'package:ressources_relationnelles/widgets/bottom-navigation-bar.widget.dart';
import 'package:ressources_relationnelles/widgets/drawer.widget.dart';

class SettingsPage extends StatefulWidget {
  SettingsPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: ResRelColors.primary, title: Text("Paramètres")),
      bottomNavigationBar: ResRelNavigation(),
      drawer: ResRelDrawer(),
      body: Container(
        child: Padding(
          padding: new EdgeInsets.all(30),
          child: (Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.center, children: []),
            ],
          )),
        ),
      ),
    );
  }
}
