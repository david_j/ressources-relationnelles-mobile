import 'dart:convert';

class User {
  final String id;
  final String username;
  final String email;
  final String firstName;
  final String lastName;
  final String birthDate;
  final String phone;
  final String genre;
  final String address;
  final String city;
  final String postalCode;
  final String profilePicture;
  final String role;

  User(
      this.id,
      this.username,
      this.email,
      this.firstName,
      this.lastName,
      this.birthDate,
      this.phone,
      this.genre,
      this.address,
      this.city,
      this.postalCode,
      this.profilePicture,
      this.role);

  User.fromJson(Map<String, dynamic> json)
      : id = json['_id'],
        username = json['username'],
        email = json['email'],
        firstName = json['firstName'],
        lastName = json['lastName'],
        birthDate = json['birthDate'],
        phone = json['phone'],
        genre = json['genre'],
        address = json['address'],
        city = json['city'],
        postalCode = json['postalCode'],
        profilePicture = json['profilePicture'],
        role = json['role'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'username': username,
        'email': email,
        'firstName': firstName,
        'lastName': lastName,
        'birthDate': birthDate,
        'phone': phone,
        'genre': genre,
        'address': address,
        'city': city,
        'postalCode': postalCode,
        'profilePicture': profilePicture,
        'role': role
      };

  @override
  String toString() {
    return 'User{id: $id, username: $username, email: $email, firstName: $firstName, lastName: $lastName, birthDate: $birthDate, phone: $phone, genre: $genre, address: $address, city: $city, postalCode: $postalCode, profilePicture: $profilePicture, role: $role}';
  }

  List<User> parseUsers(String responseBody) {
    final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

    return parsed.map<User>((json) => User.fromJson(json)).toList();
  }
}
