import 'dart:convert';

import 'User.dart';

class Relation {
  final String id;
  final User user;
  final User relationUser;
  final String title;
  final String message;
  final String image;
  final String linkUrl;
  final String createdAt;

  Relation(this.id, this.user, this.relationUser, this.title, this.message,
      this.image, this.linkUrl, this.createdAt);

  Relation.fromJson(dynamic json)
      : id = json['_id'],
        user = User.fromJson(json['user']),
        relationUser = User.fromJson(json['relationUser']),
        title = json['title'],
        message = json['message'],
        image = json['image'],
        linkUrl = json['linkUrl'],
        createdAt = json['createdAt'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'user': user,
        'relationUser': relationUser,
        'title': title,
        'message': message,
        'image': image,
        'linkUrl': linkUrl,
        'createdAt': createdAt,
      };

  @override
  String toString() {
    return 'Relation{id: $id, user: $user, relationUser: $relationUser, title: $title, message: $message, image: $image, linkUrl: $linkUrl, createdAt: $createdAt}';
  }

  List<Relation> parseRelations(String responseBody) {
    final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

    return parsed.map<Relation>((json) => Relation.fromJson(json)).toList();
  }
}
