import 'dart:convert';

import 'User.dart';

class Resource {
  final String id;
  final User user;
  final String title;
  final String message;
  final String image;
  final String linkUrl;
  final String createdAt;

  Resource(this.id, this.user, this.title, this.message, this.image,
      this.linkUrl, this.createdAt);

  Resource.fromJson(dynamic json)
      : id = json['_id'],
        user = User.fromJson(json['user']),
        title = json['title'],
        message = json['message'],
        image = json['image'],
        linkUrl = json['linkUrl'],
        createdAt = json['createdAt'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'user': user,
        'title': title,
        'message': message,
        'image': image,
        'linkUrl': linkUrl,
        'createdAt': createdAt,
      };

  @override
  String toString() {
    return 'Resource{id: $id, title: $title, message: $message, image: $image, linkUrl: $linkUrl, createdAt: $createdAt}';
  }

  List<Resource> parseResources(String responseBody) {
    final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

    return parsed.map<Resource>((json) => Resource.fromJson(json)).toList();
  }
}
